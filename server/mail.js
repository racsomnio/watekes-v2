const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  secure: true,
  // service: 'Gmail',
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASS,
  },
});

const makeANiceEmail = text => `
  <div className="email" style="
    border: 1px dashed #ccc;
    padding: 20px;
    font-family: sans-serif;
    line-height: 2;
    font-size: 12px;
  ">
    <p>Hola,</p>
    <p>${text}</p>

    <p>watekes.com</p>
    <small>Donde la diversión es cosa seria</small>
  </div>
`;

exports.transport = transport;
exports.makeANiceEmail = makeANiceEmail;
