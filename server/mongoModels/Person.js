const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const peopleSchema = new Schema({
  firstName: {
    type: String,
    trim: true,
    lowercase: true,
    required: true,
  },
  whatsApp: {
    type: Number,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    required: true,
  },
  password:{
    type: String,
    minlength: 6,
  },
  created: {
    type: Date,
    default: Date.now
  },
  location: {
    type: {
      type: String,
      default: 'Point'
    },
    coordinates: [{
      type: Number
    }],
    address: {
      type: String,
      default: "New York"
    }
  },
  resetToken: {
    type: String
  },
  resetTokenExpiry:{
    type: Date
  },
  permission: {
    type: [String],
    enum: ["ADMIN", "OPENPLAN", "FANCYPLAN", "PICKYPLAN", "PERMISSIONUPDATE"],
    default: "OPENPLAN"
  },
  stripeId:{
    type: String
  },
  image:[{
    type: String
  }],
  largeImage:[{
    type: String
  }],
  blurImage:[{
    type: String
  }],
  privateParts:[{
    type: String
  }],
  clothing:[{
    type: String
  }],
  orientation:[{
    type: String
  }],
  job:{
    type: String
  },
  dobDay:{
    type: Number
  },
  dobMonth:{
    type: Number
  },
  dobYear:{
    type: Number
  },
  rubType:[{
    type: String
  }],
  about:{
    type: String
  },
  warn:{
    type: String
  },
  sessionAt:[{
    type: String
  }],
  rubbables:[{
    type: String
  }],
  endorses:[{
    type: String
  }],
  unlikes:[{
    type: String
  }],
  rubbed:[{
    type: String
  }],
  faves:[{
    type: String
  }],
  reported:[{
    type: String
  }],
});
mongoose.set('useCreateIndex', true); // needed to avoid deprecation warning
peopleSchema.index({ location: '2dsphere' }); // needed for geoNear

module.exports = mongoose.model('Person', peopleSchema);