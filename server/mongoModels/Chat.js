const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const chatSchema = new Schema({

    membersId: [{
        type: String,
    }],
    messages: [{
        memberId:{
            type: Schema.Types.ObjectId,
            ref: 'Person'
        },
        message:{
            type: String,
            required: true,
        },
        created: {
            type: Date,
            default: Date.now
        },
    }]

});

module.exports = mongoose.model('Chat', chatSchema);