const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// const Person = require('./Person');

const convoSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    membersId:[{
        type: Schema.Types.ObjectId,
        ref: 'Person'
      }],
    chatId: {
        type: Schema.Types.ObjectId,
        ref: 'Chat'
    },
});

module.exports = mongoose.model('Convo', convoSchema);