const typeDefs = `
enum Permission {
  ADMIN
  OPENPLAN
  FANCYPLAN
  PICKYPLAN
  PERMISSIONUPDATE
}
scalar DateTime

type Person {
  _id: String
  firstName: String!
  location: Location
  email: String!
  password: String!
  whatsApp: Float
  resetToken: String
  resetTokenExpiry: Float
  permission: [Permission]
  stripeId: String
  image: [String]
  largeImage: [String]
  blurImage: [String]
  privateParts: [String]
  clothing: [String]
  orientation: [String]
  job: String
  dobDay: Int
  dobMonth: Int
  dobYear: Int
  rubType: [String]
  sessionAt: [String]
  about: String
  warn: String
  created: DateTime
  rubbables: [String]
  endorses: [String]
  unlikes: [String]
  rubbed: [String]
  faves: [String]
  reported: [String]
  distance: Float
}

input PersonInput {
  firstName: String
  location: LocationInput
  email: String
  password: String
  whatsApp: Float
  image: [String]
  largeImage: [String]
  blurImage: [String]
  privateParts: [String]
  clothing: [String]
  orientation: [String]
  job: String
  dobDay: Int
  dobMonth: Int
  dobYear: Int
  rubType: [String]
  sessionAt: [String]
  about: String
  warn: String
  rubbables: [String]
  endorses: [String]
  unlikes: [String]
  rubbed: [String]
  faves: [String]
  reported: [String]
}

type Location {
  address: String
  coordinates: [Float]
}

input LocationInput {
  type: String
  address: String
  coordinates: [Float]
}

type Convo{
  _id: String
  created: DateTime
  membersId: [Person]
  chatId: Chat
}

input ConvoInput{
  membersId: [String]
  chatId: String
}

type Chat{
  _id: String
  membersId: [String]
  messages: [ Message ]
  
}

input ChatInput{
  membersId: [String]
  messages: [ MessageInput ]
}

type Message{
  _id: String
  chatId: String!
  memberId: Person
  message: String!
  created: DateTime
}

input MessageInput{
  chatId: String!
  memberId: String
  message: String!
}

type SuccessMessage {
  message: String
}

type Query {
  peopleAround(minDistance:Float): [Person]
  person(_id: String): Person
  isLoggedIn: Person
  hasConvos: [Convo]
  chat(chatId:String!): Chat
  messages(chatId:String!):[Message]
}

type Mutation {
    sendInvitation(email: String!): SuccessMessage
    createPerson(person: PersonInput): Person!
    updatePerson(person: PersonInput): Person!
    pushToPerson(person: PersonInput): Person!
    pushImage(person: PersonInput): Person!
    pullImage(index: Int!): Person!
    deletePerson(_id: ID!): Person!
    login(email: String!, password: String!): Person!
    signout: SuccessMessage
    requestReset(email: String!): SuccessMessage
    resetPassword(resetToken: String!, password: String!, confirmPassword: String!): Person!
    createSubscription(token: String!, plan: String!): Person!
  
    createConvo(convo: ConvoInput): Convo!
    updateMessages(chatId: String, message: String!): Chat!
}

type Subscription {
  newMessage(chatId: String): Chat!
  personTyping (personId: String!): String
}
`
module.exports = typeDefs;