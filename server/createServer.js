const { GraphQLServer } = require('graphql-yoga');
const { RedisPubSub } =  require('graphql-redis-subscriptions');
const Redis = require('ioredis');
const typeDefs = require('./graphqlSchema');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');

// JUST FOR PROD
const options = {
    host: process.env.REDIS_DOMAIN_NAME,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS,
    retryStrategy: times => {
      // reconnect after
      return Math.min(times * 50, 2000);
    }
  };

const pubsub = new RedisPubSub({
    publisher: new Redis(options),
    subscriber: new Redis(options)
});

// JUST FOR DEV
// const pubsub = new RedisPubSub();

//create graphql yoga server
function createServer(){
    return new GraphQLServer({ 
        typeDefs, 
        resolvers: {
            Query,
            Mutation,
            Subscription
        },
        context: req => ({ ...req, pubsub })
    });
}

module.exports = createServer;