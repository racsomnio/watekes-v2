const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
require('dotenv').config({ path: 'variables.env'})
const mongoose = require('mongoose');
const createServer = require('./createServer');
const Person = require('./mongoModels/Person');

const db = mongoose.connect(process.env.DB, { 
  useNewUrlParser: true,
  useFindAndModify: false,
} ).catch((err) => {
  console.log("Not Connected to Database ERROR! ", err);
});;

const server = createServer();

// middleware to handle cookies in every page, like JWT
server.express.use(cookieParser());

// decode JWT from cookie
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  // console.log(token);
  if(token){
    const {userId} = jwt.verify(token, process.env.APP_SECRET);
    // put the userId onto the req for future request to access
    req.userId = userId ;
  }
  
  next();
  
});

// middleware that populates the user on each request
server.express.use(async (req, res, next) => {
  // if they aren't logged in, skip this
  if (!req.userId) return next();

  const user = await Person.findOne(
    { _id: req.userId },
    { "permission": 1, "email": 1, "location.coordinates": 1, "firstName": 1 }
    );
  console.log(user);
  req.user = user;
  next();
});

server.start({
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    },
  }, deets => {
    console.log(`server is now running on port http://localhost:${deets.port}`)
  });

  
