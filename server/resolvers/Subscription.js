const { withFilter } = require('graphql-yoga');
const PUBSUB_NEW_MESSAGE = require('./pubsubConsts').PUBSUB_NEW_MESSAGE;

const Subscription = {
    newMessage:{
        subscribe: withFilter(
            (_, __, { pubsub }, info) => pubsub.asyncIterator(PUBSUB_NEW_MESSAGE), 
            (payload, variables) => {
                console.log("is payload = variables?", payload.newMessage._id == variables.chatId)
                return (payload.newMessage._id == variables.chatId);
                // return console.log("payload: ", payload, "variables", variables)
            }),
    }
};

module.exports = Subscription;