const Person = require('../mongoModels/Person');
const Convo = require('../mongoModels/Convo');
const Chat = require('../mongoModels/Chat');

const Query = {
    peopleAround: async (parent, args, context) => {
      if (!context.request.userId) {
        throw new Error('Privacy is important to us. Please log in to see this content.');
      }
      // console.log(context.request.user.location.coordinates)
      const res = await Person.aggregate([
        {
          $geoNear: {
            near: { type: 'Point', coordinates: context.request.user.location.coordinates },
            distanceField: 'distance',
            // distanceMultiplier: 0.00062137, // 0.001 to convert to km, and 0.00062137 to miles
            minDistance: (args.minDistance || 0) + 0.001, // + 0.001 is added to avoid duplicated results
            maxDistance: 10000,
            spherical: true,
          }
        },
        { "$limit": 4 },
      ]);
      console.log(res);
      return res;
    },
    person: async (parent, args, context, info) => {
      const id = (args._id) ? args._id : context.request.userId; // If ID is empty it will show my account 
      const person = Person.findOne({ _id : id })
      return await person;
    },
    isLoggedIn: async (parent, args, context, info) => {
      const { userId } = context.request;
      // Check if there is a user ID
      // if(!userId){
      //   return null;
      // }
      const person = Person.findOne({ _id : userId })
      // console.log(person);
      return await person;
    },

    hasConvos: async (parent, args, context, info) => {
      const { userId } = context.request;
      if (!userId) throw new Error('You must be signed in to complete this order.');
      
      const convos = Convo.find({ membersId: { $in: userId }}).populate('membersId');
      return await convos;
    },

    chat: async (parent, {chatId}, context, info) => {
      const { userId } = context.request;
      // if (!userId) throw new Error('You must be signed in to complete this order.');

      const chat = await Chat.findOne({ _id: chatId}).populate('messages.memberId');
      if(!chat || !chat.membersId.includes(userId)) throw new Error('You are in the wrong chat!');

      return chat;

    },
};

module.exports = Query;

