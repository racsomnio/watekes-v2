const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Person = require('../mongoModels/Person');
const Convo = require('../mongoModels/Convo');
const Chat = require('../mongoModels/Chat');
const { randomBytes } = require('crypto'); // Buil-in node module
const { promisify } = require('util'); // Buil-in node module
const { transport, makeANiceEmail } = require('../mail');
const stripe = require('../stripe');
const PUBSUB_NEW_MESSAGE = require('./pubsubConsts').PUBSUB_NEW_MESSAGE;

const Mutations = {
    sendInvitation: async (parent, {email}, context) => {
        console.log(email);
        if(email == ''){
            throw new Error('No se escribió ningun correo electrónico.')
        }
       
        // Email them that reset token
        const mailRes = await transport.sendMail({
        from: 'alexchen@gmail.com',
        to: email,
        // to: 'oscar.uix@gmail.com',
        subject: 'Tu invitación a watekes.com',
        html: makeANiceEmail(`Has recibido una invitación para anunciarte gratis en watekes.com, solo tienes que dar clic en el siguiente link:
        \n\n
        <a href="${process.env.FRONTEND_URL}/signup">Quiero registrarme</a>`),
        });

        return { message: 'Thanks!' };
    },
    // Person ======
    createPerson: async (parent, {person}, context, info) => {
        // hash password
        person.password = (person.password.length >= 6) ? await bcrypt.hash(person.password, 10): person.password;

        const newPerson = new Person(person);
        await newPerson.save()

        // generate the JWT Token
        const token = jwt.sign({ userId: newPerson._id }, process.env.APP_SECRET);

        // Set cookie with the token        
        context.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365, // 1 year cookie
        });

        return newPerson
    },

    updatePerson: async (parent, {person}, context, info) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('You must be signed in to complete this order.');
        
        return await Person.findOneAndUpdate({_id : userId}, person);
    },

    pushToPerson: async (parent, {person}, context, info) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('You must be signed in to complete this order.');

        const res = await Person.findOneAndUpdate({_id: userId}, {
            $addToSet : person
        });

        console.log(person)
        return res;
    },

    pushImage: async (parent, {person}, context, info) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('You must be signed in to complete this order.');

        if(person.image.length > 5) throw new Error('You can upload only 5 pics, maybe delete one and try again.');

        const res = await Person.findOneAndUpdate({_id: userId}, {
            $addToSet : person
        });

        console.log(person)
        return res;
    },

    pullImage: async (parent, {index}, context, info) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('You must be signed in to complete this order.');

        const res = await Person.findOne({_id: userId});
        res.image = res.image.slice(0, index).concat(res.image.slice(index + 1, res.image.length))
        res.largeImage = res.largeImage.slice(0, index).concat(res.largeImage.slice(index + 1, res.largeImage.length))
        res.blurImage = res.blurImage.slice(0, index).concat(res.blurImage.slice(index + 1, res.blurImage.length))

        console.log(res);
        await res.save();
        return res;
    },

    deletePerson: (parent, {_id}) => {
        return new Promise( (resolve, reject) => {
            Person.findOneAndDelete({_id}, function(err, result){
                if (err) return err;
                resolve(result)
            })
        })
    },
    
    login: async (parent, {email, password}, context) => {
        // 1. check if there is a user with that email
        const user = await Person.findOne({email});
        if (!user) {
        throw new Error(`No such user found for email ${email}`);
        }
        // 2. Check if their password is correct
        const valid = await bcrypt.compare(password, user.password);
        if (!valid) {
        throw new Error('Invalid Password!');
        }
        // 3. generate the JWT Token
        const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
        // 4. Set the cookie with the token
        context.response.cookie('token', token, {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24 * 365,
        });
        console.log("=====",token);
        
        // 5. Return the user
        return user;
    },

    signout: async (parent, args, context) => {
        context.response.clearCookie('token');
        return { message: 'Goodbye!' };
    },

    requestReset: async (parent, args, context) => {
        // 1. Check if this is a real user
        const user = await Person.findOne({ email: args.email });
        if (!user) {
        throw new Error(`No such user found for email ${args.email}`);
        }
        // 2. Set a reset token and expiry on that user
        const randomBytesPromiseified = promisify(randomBytes);
        const resetToken = (await randomBytesPromiseified(20)).toString('hex');
        const resetTokenExpiry = Date.now() + 3600000; // 1 hour from now
        const res = await Person.findOneAndUpdate({email: args.email}, { resetToken, resetTokenExpiry });

        console.log(res);

        // 3. Email them that reset token
        const mailRes = await transport.sendMail({
        from: 'alexchen@gmail.com',
        // to: user.email,
        to: 'oscar.uix@gmail.com',
        subject: 'Your Password Reset Token',
        html: makeANiceEmail(`Your Password Reset Token is here!
        \n\n
        <a href="${process.env.FRONTEND_URL}/reset-password?resetToken=${resetToken}">Click Here to Reset</a>`),
        });

        return { message: 'Thanks!' };
    },

    resetPassword: async (parent, args, context) => {
        // check if the passwords match
        if (args.password !== args.confirmPassword) {
            throw new Error("Your Passwords don't match!");
        }

        // Hash the new password
        const password = await bcrypt.hash(args.password, 10);

        // check if its a legit reset token and Check if its expired
        // we query users in order to query for nodes that are not unique, like resetToken, instead of quering for ID or email.
        const user = await Person.findOneAndUpdate({
            resetToken: args.resetToken,
            resetTokenExpiry: { $gte: Date.now() - 3600000 }
            },
            {
                password,
                resetToken: null,
                resetTokenExpiry: null,
            }
        );
        // console.log(user);
        if (!user) {
            throw new Error('This token is either invalid or expired!');
        }
        
        // Generate JWT
        const token = jwt.sign({ userId: user._id }, process.env.APP_SECRET);
        
        // Set the JWT cookie
        context.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365,
        });

        // return the new user
        return user;
    },

    createSubscription: async (parent, {token, plan}, context) => {
        // Query the current user and make sure they are signed in
        const { userId } = context.request;
        if (!userId) throw new Error('You must be signed in to complete this order.');

        const user = await Person.findOne(
            { _id: userId },
            {
                firstName: 1,
                email: 1,
            }
          );
        
        console.log("plan:"+process.env[plan]);
        // create a Stripe Customer
        const customer = await stripe.customers.create({
            email: user.email,
            source: token,
            plan: process.env[plan]
        });

        console.log(customer);
        user.stripeId = customer.id;
        user.permission = [plan] ;
        await user.save();

        return user;

    },

    // Convo ======
    createConvo: async (parent, {convo}, context) => {
        const newConvo = new Convo(convo);
        const newChat = new Chat();
            newChat.membersId = newConvo.membersId;
        await newChat.save();
            newConvo.chatId = newChat._id;
        await newConvo.save();

        return newConvo
    },

    updateMessages: async (parent, {chatId, message}, {request, pubsub}, info) => {
        // Query the current user and make sure they are signed in
        const { user } = request;
        // const id = "5d4b328739ab9802f7d3c7aa";
        if (!user) throw new Error('You must be signed in to complete this order.');
        
        const res = await Chat.findOneAndUpdate({ _id: chatId}, {$addToSet: {messages: [{
            memberId: user._id,
            message
        }]}}, {new: true}).populate('messages.memberId').exec(); // use new:true to return the new data 

        pubsub.publish(PUBSUB_NEW_MESSAGE, {newMessage: res});

        return res;
    },
};

module.exports = Mutations;