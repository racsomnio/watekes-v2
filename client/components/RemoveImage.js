const RemoveImage = (props) => (
   <li>
      <img src={props.src} num={props.index} onClick={props.onClick}/> 
    </li>
  )
  
export default RemoveImage