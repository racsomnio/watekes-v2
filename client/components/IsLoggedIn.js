import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';

const IS_LOGGED_IN_QUERY = gql`
  query {
    isLoggedIn {
      _id
      email
      firstName
      permission
    }
  }
`;

const IsLoggedIn = props => (
    <Query {...props} query={IS_LOGGED_IN_QUERY}>
        {payload => props.children(payload)}
    </Query>
);

IsLoggedIn.propTypes = {
children : PropTypes.func.isRequired,
};

export default IsLoggedIn;
export { IS_LOGGED_IN_QUERY };