import React, { Component } from 'react';
import Person from './Person';
import styled from 'styled-components';
import throttle from 'lodash.throttle';

const PeopleList = styled.div`
  /* display: grid; */
  /* grid-template-columns: 1fr 1fr; */
  /* grid-gap: 60px; */
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto 20px;
`;

export default class LoadPeople extends Component {
    componentDidMount() {
        window.addEventListener("scroll", throttle(this.handleOnScroll, 3000));
    }
    
    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleOnScroll);
    }
    
    handleOnScroll = (callback) => {
        // http://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom
        var scrollTop =
          (document.documentElement && document.documentElement.scrollTop) ||
          document.body.scrollTop;
        var scrollHeight =
          (document.documentElement && document.documentElement.scrollHeight) ||
          document.body.scrollHeight;
        var clientHeight =
          document.documentElement.clientHeight || window.innerHeight;
        var scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;
        if (scrolledToBottom) {
          console.log("you hit bottom");
          this.props.onLoadMore();
        }
    };

    render() {
        return (
            <PeopleList>
                {this.props.entries.map(person => <Person key={person._id} person={person} distance={person.distance} />)}
            </PeopleList>
        )
    }
}
