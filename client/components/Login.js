import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import HoneyPot from './styles/HoneyPot';
import Form from './styles/Form';
import Error from './ErrorMessage';
import { IS_LOGGED_IN_QUERY } from './IsLoggedIn';

const LOGIN_MUTATION = gql`
  mutation LOGIN_MUTATION($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      _id
      email
      firstName
    }
  }
`;

class Login extends Component {
    state = {
        name: '',
        password: '',
        email: '',
    };

    saveToState = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render() {
        return (
            <Mutation
                mutation={LOGIN_MUTATION}
                variables={this.state}
                refetchQueries={[{ query: IS_LOGGED_IN_QUERY }]}
            >
                {(login, { error, loading }) => (
                <Form
                    method="post"
                    onSubmit={async e => {
                        e.preventDefault();
                        await login();
                        this.setState({ name: '', email: '', password: '' });
                        Router.push({
                            pathname: '/search',
                            // query: { id: res.data.createItem.id}
                        })
                    }}
                >
                    <fieldset disabled={loading} aria-busy={loading}>
                    {/* <h2>Log Into Your Account</h2> */}
                    <Error error={error} />
                    
                    <HoneyPot htmlFor="question">
                        Question
                        <input type="text" name="question" value={this.state.question} onChange={this.saveToState}/>
                    </HoneyPot>

                    <label htmlFor="email">
                        Email
                        <input
                        type="email"
                        name="email"
                        value={this.state.email}
                        onChange={this.saveToState}
                        />
                    </label>

                    <label htmlFor="password">
                        Password
                        <input
                        type="password"
                        name="password"
                        value={this.state.password}
                        onChange={this.saveToState}
                        />
                    </label>

                    <button type="submit">Let Me In</button>
                    </fieldset>
                </Form>
                )}
            </Mutation>
        )
    }
}

export default Login