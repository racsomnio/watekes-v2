import React, { Component } from 'react';
import { Mutation, Query } from 'react-apollo';
import AddPhotoIcon from '../static/icons/addPhoto.svg';
import gql from 'graphql-tag';
import { ME_QUERY } from './EditAccount';
import Error from './ErrorMessage';
import Center from './styles/Center';
import MainButton from './styles/MainButton';
import RemoveImage from './RemoveImage';
import ImageList from './styles/ImageList';

const PUSH_IMAGE_MUTATION = gql`
  mutation PUSH_IMAGE_MUTATION(
    $person: PersonInput!
  ) {
    pushImage(
      person: $person,
    ){
      _id
      image
      largeImage
      blurImage
    }
  }
`;

const PULL_IMAGE_MUTATION = gql`
  mutation PULL_IMAGE_MUTATION(
    $index: Int!
  ) {
    pullImage(
      index: $index,
    ){
      _id
    }
  }
`;

class UploadPics extends Component {
    showWidget = (num, fnMutation) => {
        return cloudinary.createUploadWidget(
            {
            cloudName: process.env.REACT_APP_CLOUD_NAME, 
                uploadPreset: process.env.REACT_APP_UPLOAD_PRESETS,
                maxFiles: 5 - num,
                sources: ['local', 'camera', 'facebook', 'instagram']
            }, 
            (error, result) => { 
                if (!error && result && result.event === "success") { 
                    console.log('Done! Here is the image info: ', result.info); 

                    fnMutation({ variables: { person: {
                        image: [result.info.eager[1].secure_url],
                        largeImage: [result.info.secure_url],
                        blurImage: [result.info.eager[0].secure_url],
                    } } })
                }
            }
        ).open()
    };  

    removeImage = (delMutation, index) => {
        console.log('removing image');
        if(confirm('Are you sure?')){
            delMutation({ variables: { index } })
        }
    }
    
    render() {
        return (
          <Query query={ME_QUERY}>
            {({ data, loading, error }) => {
              if (loading) return <p>Loading...</p>;
              if(error) return <Error error={error}/>
              if (!data.person) return <p>No Item Found for ID {this.props.id}</p>;
              // console.log(data.person);

              return (
                <Mutation 
                  mutation={PUSH_IMAGE_MUTATION}
                  refetchQueries={[{ query: ME_QUERY }]}
                >
                  {(pushImage, { loading, error }) => {
                    return (
                        <Mutation 
                            mutation={PULL_IMAGE_MUTATION}
                            refetchQueries={[{ query: ME_QUERY }]}
                        >
                            {(pullImage, { loading, error }) => (
                                <Center>
                                    <h3>Show us how nice you look, upload a pic!</h3>
                                    <p><small>Bear in mind you have up to 5 pictures to show us your nicest.</small></p>
                                    <Error error={error} />
                                    
                                    {(data.person.image.length < 5) && <MainButton id="upload_widget" onClick={() => this.showWidget(data.person.image.length, pushImage)}>
                                        <AddPhotoIcon />
                                    </MainButton>}
                                    <ImageList>
                                        {data.person.image.map( (img, i) => <RemoveImage key={i} src={img} onClick={() => this.removeImage(pullImage, i)} />)}
                                    </ImageList>

                                </Center>
                            )}
                        </Mutation>
                    )
                  }}
                </Mutation>
              )
            }}
          </Query>
        )
    }
}

export default UploadPics;