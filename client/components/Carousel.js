import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const bannerSteps = [
{
    label: 'Eres animador?',
    imgPath:
        '../static/imgs/banner/payaso.jpg',
    },
    {
    label: 'Tienes un grupo de Mariachis?',
    imgPath:
      '../static/imgs/banner/mariachi.jpg',
  },
  {
    label: 'Haces piñatas?',
    imgPath:
      '../static/imgs/banner/pinata.jpg',
  },
  {
    label: 'Fiestas temáticas?',
    imgPath:
      '../static/imgs/banner/spiderman.jpg',
  },

  {
    label: 'Equipo de sonido?',
    imgPath:
      '../static/imgs/banner/dj.jpg',
  },
];

const useStyles = makeStyles(theme => ({
  root: {
    // maxWidth: 800,
    flexGrow: 1,
    textAlign: 'center',
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default,
  },
  imgContainer:{
    position: 'relative',
  },
  img: {
    // height: 255,
    display: 'block',
    //maxWidth: 400,
    overflow: 'hidden',
    width: '100%',
  },
  text:{
      position: 'absolute',
      color: '#fff',
      bottom:'60%',
      transform: 'translateY(60%)',
      fontSize: '2rem',
      fontWeight: 'bold',
      textShadow: '0 0 10px #000',
      lineHeight: '2rem',
      width: '40%',
  },
  left: {
    left: '1rem',
  },
  right: {
    right: '1rem',
  },
  marginButton:{
    marginTop: '-10px'
  }
}));

function Carousel() {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = bannerSteps.length;

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleStepChange = step => {
    setActiveStep(step);
  };

  return (
    <div className={classes.root}>
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
        slideClassName={classes.imgContainer}
      >
        {bannerSteps.map((step, index) => (
          <div key={step.label}>
            {Math.abs(activeStep - index) <= 2 ? (
              <>  
                <img className={classes.img} src={step.imgPath} alt={step.label} />
                <Typography className={[classes.text, classes.right]}>{step.label}</Typography>
              </>
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <Button variant="contained" color="primary" href="/send-invitation" className={classes.marginButton}>
        Anúnciate GRATIS aquí!
      </Button>
      <p>Da clic en el botón de arriba para que te puedas anunciar sin ningún costo. Sólo necesitarás un correo electrónico y muchas ganas de trabajar!</p>
      <p> </p>
    </div>
  );
}

export default Carousel;