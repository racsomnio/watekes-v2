import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import LoadPeople from './LoadPeople';

const PEOPLE_AROUND_QUERY = gql`
  query PEOPLE_AROUND_QUERY($minDistance:Float) {
    peopleAround(minDistance: $minDistance) {
      _id
      firstName
      image
      resetToken
      location{
        coordinates
        address
      }
      rubbables
      endorses
      unlikes
      rubbed
      faves
      reported
      created
      distance
    }
  }
`;

const Center = styled.div`
  text-align: center;
`;

class Search extends Component {


  render() {
    return (
      <Center>
        <Query 
          variables={{minDistance: 0}}
          query={PEOPLE_AROUND_QUERY}
          // notifyOnNetworkStatusChange={true} // DONT USE IT, it repaints and scroll page to the top 
        >
            {({data, error, loading, fetchMore}) => {
                console.log(data);
                if(loading) return <p>Loading ... </p>
                if(error) return <p>Error: {error.message}</p>
                return <LoadPeople 
                        loading={loading}
                        entries={data.peopleAround}
                        onLoadMore={() => fetchMore({
                          variables: { minDistance: (data.peopleAround.slice(-1)[0].distance)},
                          updateQuery: (prevResult, { fetchMoreResult }) => {
                            const previousEntry = prevResult.peopleAround;
                            const newEntries = fetchMoreResult.peopleAround;
                            const moreMinDis = (newEntries.length && newEntries.slice(-1)[0].distance);
                            console.log(newEntries);
                            console.log(previousEntry);
                            if (!newEntries.length) return prevResult;
                            return Object.assign({}, prevResult, {
                              minDistance: moreMinDis,
                              peopleAround: [...previousEntry, ...newEntries],
                            });

                            
                          },

                          
                        })}
                      />
            }}
        </Query>
      </Center>
    )
  }
}

export default Search;
