import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'
import Title from './styles/Title'
import ItemStyles from './styles/ItemStyles'
import PushToPerson from './PushToPerson'
import NoImage from '../static/icons/no-image.svg';

export default class Person extends Component {
  static propTypes = {
    person: PropTypes.object.isRequired
  }

  render() {
    const {person} = this.props;
    return (
      <ItemStyles>
        <div className="buttonList">
          {/* <PushToPerson dbNode="rubbables" id={person._id} list={person.rubbables}>
            <ReactSVG 
              src="../static/icons/rubbable.svg" 
              beforeInjection={svg => {
                svg.classList.add('person-icon')
                svg.setAttribute('style', 'width: 20px')
              }}
            />
            <small>Rubbable</small>
            </PushToPerson> */}
          
          {/* <PushToPerson dbNode="unlikes" id={person._id} list={person.unlikes}>
            <ReactSVG 
              src="../static/icons/hands-off.svg" 
              beforeInjection={svg => {
                svg.classList.add('person-icon')
                svg.setAttribute('style', 'width: 20px')
              }}
            />
            <small>Hands Off</small>
          </PushToPerson> */}

          {/* <PushToPerson dbNode="rubbed" id={person._id} list={person.rubbed}>
            <ReactSVG 
              src="../static/icons/checked.svg" 
              beforeInjection={svg => {
                svg.classList.add('person-icon')
                svg.setAttribute('style', 'width: 20px')
              }}
            />
            <small>Checked</small>
          </PushToPerson> */}
          
          {/* <PushToPerson dbNode="faves" id={person._id} list={person.faves}>
            <ReactSVG 
              src="../static/icons/fave.svg" 
              beforeInjection={svg => {
                svg.classList.add('person-icon')
                svg.setAttribute('style', 'width: 20px')
              }}
            />
            <small>Fave</small>
          </PushToPerson> */}
          
          {/* <PushToPerson dbNode="endorses" id={person._id} list={person.endorses}>
            <ReactSVG 
                src="../static/icons/endorse.svg" 
                beforeInjection={svg => {
                  svg.classList.add('person-icon')
                  svg.setAttribute('style', 'width: 20px')
                }}
            />
            <small>Endorse</small>
          </PushToPerson> */}

          {/* <PushToPerson dbNode="reported" id={person._id} list={person.reported}>
            <ReactSVG 
              src="../static/icons/report.svg" 
              beforeInjection={svg => {
                svg.classList.add('person-icon')
                svg.setAttribute('style', 'width: 20px')
              }}
            />
            <small>Report</small>
          </PushToPerson>*/}
        </div> 
        
        {(person.image && person.image.length) ? 
          <img src={person.image[0]} alt={person.firstName} width="150px" /> 
          : 
          // <ReactSVG 
          //   src="../static/icons/no-image.svg" 
          //   beforeInjection={svg => {
          //     svg.classList.add('person-icon')
          //     svg.setAttribute('style', 'width: 150px')
          //   }}
          // />
          <NoImage/>
        }
        
        <Title>
          <Link href={{
            pathname: '/person',
            query: { id: person._id }
          }}>
            <a>{person.firstName}</a>
          </Link>
          , {Math.ceil(person.distance)} mile{person.distance > 1 && "s"}
        </Title>
      </ItemStyles>
    )
  }
}
