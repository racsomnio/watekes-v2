import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

const PUSH_TO_PERSON_MUTATION = gql`
  mutation PUSH_TO_PERSON_MUTATION(
    $person: PersonInput!
  ) {
    pushToPerson(
      person: $person,
    ){
      _id
      rubbables
      endorses
      unlikes
      rubbed
      faves
      reported
    }
  }
`;

const CREATE_CONVO_MUTATION = gql`
    mutation CREATE_CONVO_MUTATION($convo: ConvoInput){
        createConvo(convo: $convo){
            membersId{
              _id
              firstName
            }
        }
    }
`;

class Rubbable extends Component {

  render() {
    return(
      <Mutation mutation={PUSH_TO_PERSON_MUTATION}>
        {(pushToPerson, {loading, error}) => (
          <Mutation mutation={CREATE_CONVO_MUTATION}>
            {(createConvo, {loading, error}) => (
              <button onClick={
                async () => {
                  const res = await pushToPerson({ variables: { person: {[this.props.dbNode]: [this.props.id]} }});
                  console.log(res.data.pushToPerson._id);
                  // console.log(this.props.list)
                  
                  if(this.props.list && this.props.list.includes(res.data.pushToPerson._id)){
                    {alert("rubbables");}
                    const initConvo = await createConvo({variables: {
                      convo:{
                        membersId: [res.data.pushToPerson._id, this.props.id] // [user logged in, user from search]
                      }
                    }});
                    console.log(initConvo);
                    
                  }
                }}
                disabled={loading}
              >
                  {this.props.children}
              </button>
            )}
          </Mutation>
        )}
      </Mutation>
    )
  }
};

export default Rubbable;
