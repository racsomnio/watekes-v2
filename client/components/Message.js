import React, { Component } from 'react'

export default class Message extends Component {
    componentDidMount() {
       this.props.newMessage();
    }

    render() {
        return (
            <ul>
                {this.props.messages.map( message => (
                    <li key={message._id}> 
                        <span>{message.memberId.firstName} says:</span>
                        <p>{message.message}</p>
                    </li>
                ))}
            </ul>
        )
    }
}
