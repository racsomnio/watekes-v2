import styled, { keyframes } from 'styled-components';

const loading = keyframes`
  from {
    background-position: 0 0;
    /* rotate: 0; */
  }

  to {
    background-position: 100% 100%;
    /* rotate: 360deg; */
  }
`;

const Form = styled.form`
  /* padding: 20px; */
  font-size: 1.5rem;
  line-height: 1.5;
  font-weight: 600;
  text-align: center;
  label {
    display: block;
    margin-bottom: 1rem;
    background:#fff;
    padding: 5px;
    text-align: left;
    user-select: none;
  }
  .checkLabel{
    text-align: center;
    display: inline-flex;
    max-width: 50%;
  }
  input,
  textarea,
  select {
    width: 100%;
    padding: 1rem;
    border-radius: 5px;
    font-size: 2rem;
    border: 1px dotted ${props => props.theme.grey};
    margin-bottom: 1rem;
    transition: background 0.5s;
    font-family: inherit;
    font-size: inherit;
    
    &:focus {
      outline: 0;
      border-color: ${props => props.theme.mainColor};
      background: ${props => props.theme.mainColor};
      color: #fff;
    }
  }
  button,
  input[type='submit'] {
    width: auto;
    background: #a30bca;
    margin: auto;
    color: white;
    border: 0;
    font-size: 1.7rem;
    font-weight: 600;
    padding: 1rem 1.2rem;
    border-radius: 5px;
  }
  h2{
    font-size: 90%;
    text-align: center;
  }
  fieldset {
    /* box-shadow: 0 0 5px 3px rgba(0, 0, 0, 0.05); */
    border: 0;
    padding: 2rem;
    max-width: 600px;
    margin: auto auto 1rem;
    border-radius: 5px;

    &[disabled] {
      opacity: 0.5;
    }
    /* &::before {
      height: 0;
      content: '';
      display: block;
      background: #a30bca;
      border-radius:5px;
      margin-bottom: 2.5rem;
    } */
    &[aria-busy='true']::before {
      height: 3px;
      background-size: 50% auto;
      background-image: repeating-linear-gradient(to right, white 0px, white 20px, #a30bca 20px, #a30bca 40px);
      animation: ${loading} 1s linear infinite;
    }
  }
  legend{
    padding: 2rem 2rem 0;
  }
  .checkRemove{
    position: absolute;
    width:0;
    height:0;
    opacity:0;

    &:checked ~ .checkIcon{
      border-bottom: solid 5px #d0767b;
    }
  }
  .checkIcon{
    cursor: pointer;
    padding: 1rem;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    svg{
      align-self: center;
      width: 50px;
    }
  }
`;

export default Form;
