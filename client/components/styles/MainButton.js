import styled from 'styled-components';

const MainButton = styled.button`
  background: #530f7f;
  color: white;
  font-weight: 500;
  border: 0;
  border-radius: 5px;
  text-transform: capitalize;
  font-size: 1.7rem;
  padding: 0.8rem 1.5rem;
  display: block;
  transition: all 0.5s;
  margin: auto auto 20px;
  &[disabled] {
    opacity: 0.5;
  }
  svg{
    fill: #fff;
    min-width: 100px;
  }
`;

export default MainButton;
