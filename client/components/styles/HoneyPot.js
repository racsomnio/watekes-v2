import styled, { keyframes } from 'styled-components';

const HoneyPot = styled.label`
    &[for="question"]{
        position: absolute;
        margin-left: -9999px;
        opacity: 0;
        z-index: -1
    }
`;

export default HoneyPot;