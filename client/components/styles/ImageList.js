import styled from 'styled-components';

const ImageList = styled.ul`
    padding: 0;
    width: 300px;
    margin: auto;

    li{
        line-height: 0;
        list-style-type: none;
        float: left;
    }
`;

export default ImageList;