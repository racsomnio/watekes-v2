import styled from 'styled-components';

const Item = styled.div`
  background: white;
  /* border: 1px solid ${props => props.theme.offWhite}; */
  box-shadow: ${props => props.theme.bs};
  position: relative;
  margin: 0 auto 40px;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  max-width: 400px;
  img {
    align-self: start;
    /* height: 400px; */
    /* object-fit: cover; */
    /* border-radius: 5px; */
  }
  p {
    font-size: 12px;
    line-height: 2;
    font-weight: 300;
    padding: 0 3rem;
    font-size: 1.5rem;
  }
  .buttonList {
    font-family: 'quicksand';
    flex-grow:1;
    display: grid;
    width: 40%;
    border-top: 1px solid ${props => props.theme.lightgrey};
    grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
    grid-gap: 1px;
    background: ${props => props.theme.lightgrey};
    & > * {
      background: white;
      border: 0;
      font-size: 1rem;
      padding: 1rem;
    }
  }
`;

export default Item;
