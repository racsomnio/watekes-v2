import styled from 'styled-components';

const Title = styled.h3`
  width: 100%;
  margin: 0;
  background: #FFF;
  border-top: 1px solid ${props => props.theme.lightgrey}
  text-align: center;
  text-transform: capitalize;
  /* transform: skew(-5deg) rotate(-1deg); */
  margin-top: -0.5rem;
  /* text-shadow: 2px 2px 0 rgba(0, 0, 0, 0.1); */
  padding: 1rem;
  /* line-height:0.8rem; */
  font-size:1.5rem;
  a {
    /* background: ${props => props.theme.red}; */
    display: inline;
    /* font-size: 4rem; */
    text-align: center;
    color: #696767;
    margin-top:-0.5rem
  }
`;

export default Title;
