import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import PropTypes from 'prop-types';
import HoneyPot from './styles/HoneyPot';

import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import Error from './ErrorMessage';
import { IS_LOGGED_IN_QUERY } from './IsLoggedIn';

const CREATE_PERSON_MUTATION = gql`
  mutation CREATE_PERSON_MUTATION($person: PersonInput!){
    createPerson(person: $person){
        _id
        email
        firstName
        whatsApp
    }
  }
`;

const styles = theme => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  });

class Signup extends Component {
    state = {
        location: {
            address : '',
            coordinates: [0,0]
        }
    };

    saveToState = (e) => {
        const val = e.target.type === 'number' ? parseFloat(e.target.value) : e.target.value;
        this.setState({ [e.target.name] : val });
        console.log(this.state);
    };

    render() {
        const { classes } = this.props;
        return (
            <Mutation
                mutation={CREATE_PERSON_MUTATION}
                // variables={this.state}
                refetchQueries={[{ query: IS_LOGGED_IN_QUERY }]}
            >
                {(createPerson, {error, loading}) => {
                    return(
                        <Container component="main" maxWidth="xs">
                            <div className={classes.paper}>
                                <Avatar className={classes.avatar}>
                                    <LockOutlinedIcon />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Invitación Gratuita
                                </Typography>
                            </div>
                            <form  
                                className={classes.form} 
                                method="post" 
                                noValidate
                                onSubmit={async e => {
                                    e.preventDefault();
                                    const res= await createPerson({ variables: { person: this.state } });
                                    // console.log(res);
                                    console.log(this.state);
                                    // ADD HERE SUCCESS MESSAGE ******
                                    this.setState({ email: '', password: '', firstName: '', whatsApp: 0});
                                    // console.log(res.data.createPerson._id)
                                    Router.push({
                                        pathname: '/activate-location',
                                        // query: { id: res.data.createPerson._id}
                                    })
                            }}>
                                {/* <fieldset disabled={loading} aria-busy={loading}> */}
                                    {/* <legend>Create an account</legend> */}
                                    <Error error={error}/>
                                    <HoneyPot htmlFor="question">
                                        Question
                                        <input type="text" name="question" value={this.state.question} onChange={this.saveToState}/>
                                    </HoneyPot>

                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Escribe tu correo electrónico"
                                        name="email"
                                        autoComplete="email"
                                        autoFocus
                                        // value={this.state.email} 
                                        onChange={this.saveToState}
                                    />


                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    Enviar
                                </Button>
                                {/* </fieldset> */}
                            </form>
                        </Container>
                    )}
                }
            </Mutation>
        )
    }
}

Signup.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(Signup);
// export default Signup