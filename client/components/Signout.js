import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import SignoutIcon from '../static/icons/unlike.svg';
import { IS_LOGGED_IN_QUERY } from './IsLoggedIn';

const SIGN_OUT_MUTATION = gql`
  mutation SIGN_OUT_MUTATION {
    signout {
      message
    }
  }
`;

const Signout = props => (
  <Mutation mutation={SIGN_OUT_MUTATION} refetchQueries={[{ query: IS_LOGGED_IN_QUERY }]}>
    {signout => <button onClick={signout}><SignoutIcon/></button>}
  </Mutation>
);
export default Signout;