import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import styled from 'styled-components';
import Router from 'next/router';
import LocationIcon from '../static/icons/location.svg';
import MainButton from './styles/MainButton';
import {UPDATE_PERSON_MUTATION} from './EditAccount';

const CenterContent = styled.div`
    text-align: center;
    max-width: 300px;
    margin: auto;
`;

export default class AskLocation extends Component {
    state = {
        message: "",
    };

    componentDidMount(){
        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition( async (pos) => {
                const coords = pos.coords;
                console.log(coords)
                
                // const geocodeUrl =`https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords.latitude},${coords.longitude}&key=${process.env.REACT_APP_MAP_KEY}`;
                // const geoName = await fetch(geocodeUrl);
                // const json = await geoName.json()
                // console.log(json.results);

                this.setState({
                    message: "Continue",
                    location: {
                        type: 'Point',
                        // address: json.results[7].formatted_address,
                        coordinates: [coords.longitude, coords.latitude] 
                    }
                })
                
            },
            async (err) => {
                console.warn(`ERROR(${err.code}): ${err.message}`);
            }
            )
        }
        
        Router.prefetch('/edit-account');
    }

    handleClick = async (updateIt) => {
        const res = await updateIt({variables:{
            person:{
             location : this.state.location
            }},
        }).catch(err => {
            alert(err.message);
        });

        Router.push({
            pathname: '/edit-account',
        })
    }

    render() {
        return (
            <Mutation mutation={UPDATE_PERSON_MUTATION}>
                {(updatePerson, { loading, error }) => {
                    
                    return (
                        <CenterContent>
                            <p>We need to know your location in order to show you fellow rubbers around.</p>
                            <p><small>Please click on the Allow button.</small></p>
                            <LocationIcon/>
                            {this.state.message !== "" && 
                                <MainButton 
                                    onClick={() => {
                                        this.handleClick(updatePerson)
                                    }}
                                >
                                    {this.state.message}
                                </MainButton>
                            }
                        </CenterContent>
                    )
                }}
            </Mutation>
        )
    }
}
