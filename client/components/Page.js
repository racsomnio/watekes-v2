import React, { Component } from 'react';
import Header from './Header';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import theme from '../lib/theme';

class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Header/>
        {this.props.children}
      </ThemeProvider>
    );
  }
}
export default Page;
