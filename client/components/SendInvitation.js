import React, { Component } from 'react'
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { green } from '@material-ui/core/colors';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import Error from './ErrorMessage';

const SEND_INVITATION_MUTATION = gql`
  mutation SEND_INVITATION_MUTATION($email: String!){
    sendInvitation(email: $email){
        message
    }
  }
`;

const styles = theme => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    question:{
        position: 'absolute',
        marginLeft: '-9999px',
        opacity: '0',
        zIndex: '-1',
    },
    close: {
        padding: theme.spacing(0.5),
    },
    success: {
        backgroundColor: green[600],
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    icon: {
        fontSize: 20,
        opacity: 0.9,
        marginRight: theme.spacing(1),
      },
});

class SendInvitation extends Component {
    state = {
        snackbarIsOpen : false,
        emailSaved : '',
        email: '',
        question: '',
    };

    saveToState = (e) => {
        const val = e.target.value;
        this.setState({ [e.target.name] : val });
        console.log(this.state)
    };

    render() {
        const { classes } = this.props;
        return (
            <Mutation
                mutation={SEND_INVITATION_MUTATION}
                variables={this.state}
            >
                {(sendInvitation, {error, loading}) => {
                    return(
                        <Container component="main" maxWidth="xs">
                            <div className={classes.paper}>
                                <Avatar className={classes.avatar}>
                                    <LockOutlinedIcon />
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Invitación Gratuita
                                </Typography>
                            </div>
                            <form  
                                className={classes.form} 
                                method="post" 
                                noValidate
                                onSubmit={async e => {
                                    e.preventDefault();
                                    await sendInvitation();
                                    this.setState({
                                        emailSaved: this.state.email,
                                        snackbarIsOpen: true, 
                                        email: ''
                                    })
                            }}>
                                {/* <fieldset disabled={loading} aria-busy={loading}> */}
                                    {/* <legend>Create an account</legend> */}
                                    <Error error={error}/>
                                    <TextField
                                        value={this.state.question} 
                                        variant="outlined"
                                        className={classes.question}
                                        margin="normal"
                                        fullWidth
                                        id="question"
                                        label="Question"
                                        name="question"
                                        onChange={this.saveToState}
                                    />

                                    <TextField
                                        value={this.state.email}
                                        type="email" 
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Escribe tu correo electrónico"
                                        name="email"
                                        onChange={this.saveToState}
                                    />

                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    Enviar
                                </Button>
                                {/* </fieldset> */}
                            </form>

                            <Snackbar
                                anchorOrigin={{ vertical:'bottom', horizontal:'center' }}
                                key={`bottom,center`}
                                open={this.state.snackbarIsOpen}
                                ContentProps={{
                                    'aria-describedby': 'message-id',
                                }}
                                
                            >
                                <SnackbarContent
                                  className = {classes.success}
                                  message={
                                   <span id="client-snackbar" className={classes.message}>
                                      <CheckCircleIcon className={classes.icon}/>
                                      <span id="message-id">
                                        Hemos enviado una invitación a:
                                        <br/>
                                        <strong>{this.state.emailSaved}</strong>
                                      </span>
                                   </span>
                                  }
                                />
                            </Snackbar>
                        </Container>
                    )}
                }
            </Mutation>
        )
    }
}

SendInvitation.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(SendInvitation);
// export default Signup