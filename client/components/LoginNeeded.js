import { Query } from 'react-apollo';
import { IS_LOGGED_IN_QUERY } from './IsLoggedIn';
import Login from './Login';

const PleaseSignIn = props => (
  <Query query={IS_LOGGED_IN_QUERY}>
    {({ data, loading }) => {
      if (loading) return <p>Loading...</p>;
      if (!data.isLoggedIn) {
        return (
          <div>
            <p>Please Sign In before Continuing</p>
            <Login />
          </div>
        );
      }
      return props.children;
    }}
  </Query>
);

export default PleaseSignIn;
