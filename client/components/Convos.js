import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Link from 'next/link'
import Error from './ErrorMessage';
import { IS_LOGGED_IN_QUERY } from './IsLoggedIn';

const HAS_CONVOS_QUERY = gql`
    query HAS_CONVOS_QUERY{
        hasConvos{
            membersId{
                _id
                firstName
                image
            }
            chatId{
                _id
            }
        }
    }
`;

export default class Convos extends Component {
    
    render() {
        return (
            <Query query={HAS_CONVOS_QUERY}>
                {({error, loading, data, client}) => {
                    if(error) return <Error error={error}/>
                    if(loading) return <p>Loading...</p>
                    if(!data.hasConvos.length) return <p>No convos yet</p>
                    console.log(data.hasConvos);
                    // console.log(client.readQuery({ query: IS_LOGGED_IN_QUERY }).isLoggedIn._id)
                    
                    const userId = client.readQuery({ query: IS_LOGGED_IN_QUERY }).isLoggedIn._id;
                    const chats = data.hasConvos.map((convo) => convo.membersId.filter(
                        member => {
                            return member._id !== userId
                        }
                    ));

                    return console.log(chats) || (
                        <ul>
                            {chats.map((chat, index) => (
                                <li key={data.hasConvos[index].chatId._id}>
                                    <Link href={{
                                        pathname: '/chat',
                                        query: { id: data.hasConvos[index].chatId._id }
                                    }}>
                                        <a>
                                            {
                                                chat.map(
                                                    (members) => <span key={members}><img src={members.image}/>{members.firstName}</span>
                                                )
                                            }
                                        </a>
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    )
                }}
            </Query>
        )
    }
}
