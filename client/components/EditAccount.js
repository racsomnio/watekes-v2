import React, { Component } from 'react';
import { Mutation, Query } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
// import LocationIcon from '../static/icons/location.svg';
import Form from './styles/Form';
import Error from './ErrorMessage';

const ME_QUERY = gql`
  query ME_QUERY{
    person{
      firstName
      image
      largeImage
      blurImage
      privateParts
      clothing
      orientation
      job
      dobDay
      dobMonth
      dobYear
      rubType
      sessionAt
      about
      warn
    }
  }
`;

const UPDATE_PERSON_MUTATION = gql`
  mutation UPDATE_PERSON_MUTATION(
    $person: PersonInput!
  ) {
    updatePerson(
      person: $person,
    ){
      _id
    }
  }
`;

const myBody = [
  {value: "penis", text: "penis"}, 
  {value: "vagina", text:"vagina"}, 
  {value: "boobs", text:"boobs"},
];

const myCloths = [
  {value: "woman-cloths", text: "a Woman"}, 
  {value: "man-cloths", text:"a Man"}, 
];

const myOrientation = [
  {value: "lesbian", text: "lesbian", desc:"woman attracted to woman"}, 
  {value: "gay", text:"gay", desc:"man attracted to man"}, 
  {value: "bi", text:"bisexual", desc:"self attracted to woman and man"},
  {value: "poly", text:"polysexual", desc:"self attracted to more than 2 genders"},
  {value: "pan", text:"pansexual", desc:"self attracted to all genders "}, 
  {value: "skolio", text:"skoliosexual", desc:"self attracted to all non-binar genders"}, 
  {value: "auto", text:"autosexual", desc:"prefer masturbation"}, 
  {value: "hetero", text:"heterosexual", desc:"self attracted to opposite gender"}, 
  {value: "question", text:"questioning", desc:"Not sure yet what attracts me"}, 
  {value: "coming-out", text:"Coming out", desc:"I decided to express myself as I am"}, 
  {value: "closeted", text:"Closeted", desc:"Your pictures will be blurry and show only on request "}, 
  {value: "gyne", text:"Gynesexual", desc:"Breasts, vaginas, and femininity turn me on"}, 
  {value: "andro", text:"Androsexual", desc:"Someone with penis and/or masculinity turn me on"},
  {value: "demi", text:"Demisexual", desc:"sexually attracted only if there are strong feelings"},
  {value: "socio", text:"Sociosexual", desc:"sexual activity outside of a committed relationship, picture will be blurry and show only on request"},
  {value: "sapio", text:"Sapiosexual", desc:"Self attracted to intelligence *"},
  {value: "hyper", text:"Hypersexual", desc:"Sex addicted"},
  {value: "ace", text:"asexual", desc:"attracted to none of the genders"}, 
];

const myRub = [
  {value: "casual", text: "Casual", desc:"Someone who wants to practice and learn together."}, 
  {value: "serious", text:"Serious", desc:"I'm quasi a pro, I'm expecting the same"}, 
  {value: "guru", text:"A Guru", desc:"Someone who can teach me with love and patience"},
  {value: "disciple", text:"A Disciple", desc:"Someone who wants to learn and be committed"},
];

const mySession = [
  {value: "home", text: "At my place"}, 
  {value: "hotel", text:"In a hotel"}, 
  {value: "beach", text:"On the beach"},
  {value: "park", text:"In a park"},
  {value: "spa", text:"In a Spa"},
];

class EditAccount extends Component {
    state = {};    

    componentDidMount(){
      Router.prefetch('/upload-pics');
    }

    handleChange = (e, hasData) => {
        const { name, type, value } = e.target;
        // console.log(e.target.type);
        switch (e.target.type) {
            case 'number':
                this.setState({
                    [name] : parseFloat(value)
                })
                break;

            case 'checkbox':
                const { checked } = e.target;
                if(checked){
                  // console.log(hasData.length)
                    this.setState({
                        // [name] : (hasData.length) ? [...hasData, value] : (this.state[name])? [...this.state[name], value] : [value]
                        [name] : (this.state[name]) ? (this.state[name])? [...this.state[name], value] : [value] : hasData ? [...hasData, value] : [value]
                    })
                }else{
                    this.setState({
                        [name] :  (this.state[name]) ? this.state[name].filter( current => current !== value ) : [...hasData].filter( current => current !== value )
                    })
                }
                break;

            default:
              // console.log(this.props.req);

                this.setState({
                    [name] : value
                })
        }

    };

    createCheckbox = (array, name, callback, hasData) => {
      return array.map( item => {
        const isChecked = hasData && hasData.includes(item.value);
        return (
        <label className="checkLabel" key={item.value} >
          <input type="checkbox" className="checkRemove" name={name} value={item.value} defaultChecked={isChecked} onChange={(e) => callback(e, hasData)} /> 
          <span className="checkIcon">
            {/* <ReactSVG src={`../static/icons/${item.value}.svg`} /> */}
            <span>{item.text}</span>
            {item.desc && <p><small>{item.desc}</small></p>}
          </span>
        </label>
        )
      })
    };

    render() {
        return (
          <Query query={ME_QUERY}>
            {({ data, loading, error }) => {
              if (loading) return <p>Loading...</p>;
              if(error) return <Error error={error}/>
              if (!data.person) return <p>No Item Found for ID {this.props.id}</p>;
              // console.log(data.person);
              return (
                <Mutation 
                  mutation={UPDATE_PERSON_MUTATION}
                  refetchQueries={[{ query: ME_QUERY }]}
                >
                  {(updatePerson, { loading, error }) => (
                    <Form data-test="form" onSubmit={
                        async e => {
                          e.preventDefault();
                          const res = await updatePerson({ variables: { person: this.state } });
                          // console.log(res);
                          Router.push({
                            pathname: '/upload-pics',
                            // query: { id: res.data.createItem.id}
                          })
                        }
                      }>
                        <Error error={error} />


                        <fieldset disabled={loading} aria-busy={loading}>
                          <legend>Physically, my body has:</legend>
                          {this.createCheckbox(myBody, "privateParts", this.handleChange, data.person.privateParts)}
                        </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}>
                          <legend>I dress like:</legend>
                          {this.createCheckbox(myCloths, "clothing", this.handleChange, data.person.clothing)}
                          </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}>
                          <legend>I consider myself:</legend>
                          {this.createCheckbox(myOrientation, "orientation", this.handleChange, data.person.orientation)}
                        </fieldset>
                        
                        <fieldset disabled={loading} aria-busy={loading}>
                            <legend>I work as:</legend>
                                <input
                                type="text"
                                id="job"
                                name="job"
                                defaultValue={data.person.job}
                                onChange={this.handleChange}
                                />
                        </fieldset>
                        
                        <fieldset disabled={loading} aria-busy={loading}>
                          <legend>My date of birth is:</legend>
                            <input
                              type="number"
                              id="dobMonth"
                              name="dobMonth"
                              required
                              placeholder="Month"
                              min="1" max="12"
                              defaultValue={data.person.dobMonth}
                              onChange={this.handleChange}
                            />

                            <input
                              type="number"
                              id="dobDay"
                              name="dobDay"
                              required
                              placeholder="Day"
                              min="1" max="31"
                              defaultValue={data.person.dobDay}
                              onChange={this.handleChange}
                            />
                            
                            <input
                              type="number"
                              id="dobYear"
                              name="dobYear"
                              required
                              placeholder="Year"
                              defaultValue={data.person.dobYear}
                              onChange={this.handleChange}
                            />
                        </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}>
                            <legend>My rubby might be:</legend> 
                            {this.createCheckbox(myRub, "rubType", this.handleChange, data.person.rubType)}
                        </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}>
                            <legend >I should mention that:</legend>
                            <textarea
                              type="text"
                              id="about"
                              name="about"
                              placeholder="i.e. I love massages"
                              defaultValue={data.person.about}
                              onChange={this.handleChange}
                            />
                        </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}>   
                            <legend>We can do a session:</legend> 
                            {this.createCheckbox(mySession , "sessionAt", this.handleChange, data.person.sessionAt)}
                        </fieldset>

                        <fieldset disabled={loading} aria-busy={loading}> 
                            <legend>I should warn you though:</legend>
                            <textarea
                              type="text"
                              id="warn"
                              name="warn"
                              placeholder="i.e. I always want more"
                              defaultValue={data.person.warn}
                              onChange={this.handleChange}
                            />

                          <button type="submit">Save</button>
                        </fieldset>
                    </Form>
                  )}
                </Mutation>
              )
            }}
          </Query>
        )
    }
}

export default EditAccount;
export {UPDATE_PERSON_MUTATION};
export {ME_QUERY};