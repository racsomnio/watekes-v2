import React, { Component } from 'react'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import { format, parseISO } from 'date-fns';
import Error from './ErrorMessage'
import styled from 'styled-components'

const PERSON_QUERY = gql`
  query PERSON_QUERY($_id: String) {
    person(_id: $_id) {
      _id
      firstName
      largeImage
      privateParts
      clothing
      rubType
      created
      about
      warn
      dobDay
      dobMonth
      dobYear
      job
      orientation
      sessionAt
      location{
        address
      }
    }
  }
`;

const PersonFullStyles = styled.div`
  max-width: 1200px;
  margin: 2rem auto;
  box-shadow: ${props => props.theme.bs};
  display: grid;
  grid-auto-columns: 1fr;
  grid-auto-flow: column;
  min-height: 800px;
  img{
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
  details{
    margin: 3rem;
    font-size: 2rem;
  }
`;

export default class PersonFull extends Component {
    render() {
        return (
            <Query query={PERSON_QUERY} variables={{_id: this.props.id}}>
                {({error, loading, data}) => {
                    if(error) return <Error error={error}/>
                    if(loading) return <p>Loading...</p>
                    if(!data.person) return <p>No item found for {this.props.id}</p>
                    console.log(data);
                    const person = data.person;
                    return (
                        <PersonFullStyles>
                            <img src={person.largeImage[0]} alt={person.firstName}/>
                            <div className="details">
                                <h2>{person.firstName}, {person.orientation}</h2>
                                <p>{person.job}</p>
                                <small>Member since {format(person.created, 'MMMM d, YYYY')}</small>
                                <p><small>{person.location.address}</small></p>
                                <ul>My privates: {person.privateParts.map( part => <li>{part}</li> )}</ul>
                                <ul>I dress like: {person.clothing.map( cloth => <li>{cloth}</li> )}</ul>
                                <p>I should mention that {person.about}</p>
                                <ul>Lookin for: {person.rubType.map( type => <li>{type}</li> )}</ul>
                                <ul>And we can try a session at: {person.sessionAt.map( at => <li>{at}</li> )}</ul>
                                <p>I should warn you though, {person.warn}</p>
                            </div>
                        </PersonFullStyles>
                    )
                }}
            </Query>
        )
    }
}

export { PERSON_QUERY };
