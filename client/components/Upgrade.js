import React , { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { Mutation } from 'react-apollo';
import Router from 'next/router';
import NProgress from 'nprogress';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import calcTotalPrice from '../lib/calcTotalPrice';
import Error from './ErrorMessage';
import IsLoggedIn, { IS_LOGGED_IN_QUERY } from './IsLoggedIn';

const CREATE_SUBSCRIPTION_MUTATION = gql`
  mutation createSubscription($token: String!, $plan: String!) {
    createSubscription(token: $token, plan: $plan) {
      _id
      email
    }
  }
`;

class Upgrade extends Component {
  onToken = async (res, createSubscription) => {
    // console.log(res);
    NProgress.start();
    // manually call the mutation once we have the stripe token
    const order = await createSubscription({
      variables: {
        token: res.id,
        plan: this.props.plan,
      },
    }).catch(err => {
      alert(err.message);
    });
    // Router.push({
    //   pathname: '/order',
    //   query: { id: order.data.createSubscription.id },
    // });
  };
  render() {
    return (
      <IsLoggedIn>
        {({ data: { isLoggedIn }, loading }) => {
          if (loading) return null;
          return (
            <Mutation
              mutation={CREATE_SUBSCRIPTION_MUTATION}
              refetchQueries={[{ query: IS_LOGGED_IN_QUERY }]}
            >
              {createSubscription => (
                <StripeCheckout
                  amount= {1000}
                  name="Test"
                  description="test description"
                //   image={me.cart.length && me.cart[0].item && me.cart[0].item.image}
                  stripeKey="pk_test_dP6fsJK7iZqixBfjtiOagbrX00p9xQPPHg"
                  currency="USD"
                  email={isLoggedIn.email}
                  token={res => this.onToken(res, createSubscription)}
                >
                  {this.props.children}
                </StripeCheckout>
              )}
            </Mutation>
          );
        }}
      </IsLoggedIn>
    );
  }
}

export default Upgrade;
export { CREATE_SUBSCRIPTION_MUTATION };
