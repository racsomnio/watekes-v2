import Head from 'next/head';
import theme from '../lib/theme';

const Meta = () => (
  <Head>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <meta charSet="utf-8"/>
    {/* PWA primary color */}
    <meta name="theme-color" content={theme.palette.primary.main} />
    <link rel="shorcut icon" href="/static/icons/watekes_ico.png"/>
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
    />
    <link rel="stylesheet" href="/static/nprogress.css" type="text/css"/>
    <title>Watekes</title>
  </Head>
)

export default Meta;
