import Link from 'next/link';
import styled from 'styled-components';
import IsLoggedIn from './IsLoggedIn';
import NavStyles from './styles/NavStyles';
import Signout from './Signout';
// SVGs
import LogoSvg from '../static/icons/watekes.svg';
import SearchIcon from '../static/icons/search.svg';
import AccountIcon from '../static/icons/account.svg';


const Logo = styled.div`
  text-align: center;
  svg{
    height: 60px;
  }
  /* background: ${props => props.theme.red}; */
`;

const Nav = () => (
  
    <IsLoggedIn>
      { ({data: {isLoggedIn}}) => (
        <>
          <Logo>
            <Link href="/">
              <a>
                <LogoSvg/>
              </a>
            </Link>
          </Logo>

          <NavStyles>
            { isLoggedIn && (
              <>
                {/* <Link href="/chronicles">
                  <a><ReactSVG src="../static/icons/chronicles.svg" /></a>
                </Link> */}
                
                <Link href="/search">
                  <a><SearchIcon/> Buscar</a>
                </Link>

                <Link href="/account">
                  <a><AccountIcon/> </a>
                </Link>
                
                <Link href="/signout">
                  <Signout/>
                </Link> 
              </>
            )}
            

            { !isLoggedIn && (
              <>
                <Link href="/login">
                  <a>Log in</a>
                </Link>

                <Link href="/signup">
                  <a>Sign Up</a>
                </Link>
              </>
            )}

          </NavStyles>
        </>
      )}
    </IsLoggedIn>


);

export default Nav;
