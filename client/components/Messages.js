import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Error from './ErrorMessage';
import { NEW_MESSAGE_SUBSCRIPTION } from './Chat';
import Message from './Message';


const CHAT_QUERY = gql`
    query CHAT_QUERY($chatId: String!){
        chat(chatId: $chatId){
            messages{
                memberId{
                    _id
                    firstName
                }
                message
            }
        }
    }
`;

export default class Messages extends Component {

    render() {
        return (
            <Query 
                query={CHAT_QUERY} 
                variables={{chatId: this.props.chatId}}
                // ssr={false}
                // notifyOnNetworkStatusChange={true}
            >
                {({error, loading, data, subscribeToMore}) => {
                    if(error) return <Error error={error}/>
                    if(loading) return <p>Loading...</p>

                    return (
                        <Message 
                            messages={data.chat.messages} 
                            newMessage={() => subscribeToMore({
                                document: NEW_MESSAGE_SUBSCRIPTION,
                                variables: { chatId: this.props.chatId },
                                updateQuery: (prev, {subscriptionData}) => {
                                    if(!subscriptionData) return prev;
                                    // console.log("prev:", prev, "subscriptionData:", subscriptionData)
                                    const { newMessage } = subscriptionData.data;
                                    const lastMessage = newMessage.messages[newMessage.messages.length -1];
                                    
                                    // console.log(
                                    //     {...prev, messages: [...prev.chat.messages, lastMessage]}
                                    // )
                                    return {
                                        ...prev, 
                                        chat: {
                                        messages: [...prev.chat.messages, lastMessage],
                                        __typename: prev.chat.__typename,
                                        }
                                    }
        
                                } 
                            })}
                        />
                    )
                        
                    
                }}
            </Query>
        )
    }
}
