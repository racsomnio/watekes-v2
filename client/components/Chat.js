import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query, Subscription } from 'react-apollo';
import NewMessage from './NewMessage';
import Messages from './Messages';

const NEW_MESSAGE_SUBSCRIPTION = gql`
    subscription NEW_MESSAGE_SUBSCRIPTION($chatId: String!){
        newMessage(chatId : $chatId){
            messages{
                memberId{
                    _id
                    firstName
                }
                message
            }
        }
    }
`;

export default class Chat extends Component {
    render() {
        return (
            <Subscription 
                subscription={NEW_MESSAGE_SUBSCRIPTION}
                variables={{chatId: this.props.chatId}}

            >
                { ({data}) => (
                    <>
                        <Messages chatId={this.props.chatId}/>
                        <NewMessage chatId={this.props.chatId}/>
                    </>
                )}
            </Subscription>
        )
    }
}

export {NEW_MESSAGE_SUBSCRIPTION}