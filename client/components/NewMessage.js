import React, { Component } from 'react'
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import Error from './ErrorMessage';
import Form from './styles/Form';


const UPDATE_MESSAGES_MUTATION = gql`
    mutation UPDATE_MESSAGES_MUTATION($chatId: String, $message: String!){
        updateMessages(chatId: $chatId, message: $message){
            membersId
            messages{
                _id
                memberId{
                    _id
                    firstName
                }
                message
            }
        }
    }
`;

export default class NewMessage extends Component {
    state = {
        message: '',
    }

    handleChange = (e) => {
        this.setState({
            message: e.target.value,
        })
    }

    render() {
        return (
            <Mutation 
            mutation={UPDATE_MESSAGES_MUTATION}
        >
            { (updateMessages, { loading, error }) => (
                <Form onSubmit={async e => {
                    e.preventDefault();
                    const res = await updateMessages({ variables: { 
                        chatId: this.props.chatId,
                        message: this.state.message
                    } });
                    this.setState({message: ''});
                    // console.log(res)
                    return res;
                }}>
                    <Error error={error} />
                    <input type="text" value={this.state.message} onChange={this.handleChange} />
                    <button type="submit">Send</button>
                </Form>

            )}
            </Mutation>
        )
    }
}
