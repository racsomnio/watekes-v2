import withApollo from 'next-with-apollo';
import { InMemoryCache, HttpLink } from 'apollo-boost';
import { ApolloClient } from 'apollo-client';
import { endpoint, prodEndpoint, devWs, prodWs } from '../config';
import { split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

const cache = new InMemoryCache();

const httpLink = (headers) => {
  return new HttpLink({
    uri: process.env.NODE_ENV === 'development' ? endpoint : prodEndpoint,
    headers,
    credentials: 'include', // because backend is on different domain
  })
}

// Make sure the wsLink is only created on the browser. 
// The server doesn't have a native implementation for websockets
const wsLink = process.browser
                ? new WebSocketLink({
                  uri: process.env.NODE_ENV === 'development' ? devWs : prodWs,
                  options: {
                    reconnect: true
                  }
                })
                : () => {
                  console.log('SSR');
                };

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = (headers) => {
  return split(
    // split based on operation type
    ({ query }) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      );
    },
    wsLink,
    httpLink(headers),
  )
};

const createClient = ({ headers }) => {
  return new ApolloClient({
    cache,
    link: link(headers),
  });
}

// With Apollo Boost
// function createClient({ headers }) {
//   return new ApolloClient(
//     {
//       uri: process.env.NODE_ENV === 'development' ? endpoint : endpoint,
//       request: operation => {
//         operation.setContext({
//           fetchOptions: {
//             credentials: 'include',
//           },
//           headers,
//           connectToDevTools: true,
//         });
//       }
//     }
//   );
// }

export default withApollo(createClient);
