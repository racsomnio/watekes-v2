const { parsed: localEnv } = require('dotenv').config()
const webpack = require('webpack')

module.exports = {
  webpack(config) {
    config.plugins.push(new webpack.EnvironmentPlugin(localEnv));
    
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });
    
    return config
  },
  env: {
    'REACT_APP_MAP_KEY': process.env.REACT_APP_MAP_KEY,
    'REACT_APP_CLOUD_NAME': process.env.REACT_APP_CLOUD_NAME,
    'REACT_APP_UPLOAD_PRESETS': process.env.REACT_APP_UPLOAD_PRESETS
  }
}



    
