import Upgrade from '../components/Upgrade';
import MainButton from '../components/styles/MainButton';

const UpgradePage = props => (
    <div>
      {/* Set "plan" prop same as permission from DB, and same as ENV VARS */}
      <Upgrade plan="FANCYPLAN">
        <MainButton >Go Fancy</MainButton>
      </Upgrade>

      <Upgrade plan="PICKYPLAN">
        <MainButton >I'm Picky</MainButton>
      </Upgrade>
    </div>
  )
  
  export default UpgradePage;