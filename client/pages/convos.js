import Convos from '../components/Convos';
import LoginNeeded from '../components/LoginNeeded';

const ConvosPage = () => (
    <LoginNeeded>
      <Convos/>
    </LoginNeeded>
  )
  
  export default ConvosPage