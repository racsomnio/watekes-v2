import Chat from '../components/Chat';
import LoginNeeded from '../components/LoginNeeded';

const ChatPage = (props) => (
    <LoginNeeded>
      <Chat chatId={props.query.id}/>
    </LoginNeeded>
  )
  
  export default ChatPage