import Search from '../components/Search';
import LoginNeeded from '../components/LoginNeeded';

const Index = () => (
    <LoginNeeded>
      <Search/>
    </LoginNeeded>
  )
  
  export default Index