import ResetPassword from '../components/ResetPassword';

const ResetPasswordPage = props => (
  <div>
    <p>Reset your password {props.query.resetToken}</p>
    <ResetPassword resetToken={props.query.resetToken} />
  </div>

)
export default ResetPasswordPage;