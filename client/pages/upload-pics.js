import LoginNeeded from '../components/LoginNeeded';
import UploadPics from '../components/UploadPics';
import ReactDependentScript from 'react-dependent-script';

const UploadPicsPage = () => (
    <LoginNeeded>
        <ReactDependentScript
            loadingComponent={<div>loading...</div>}
            scripts={['https://widget.cloudinary.com/v2.0/global/all.js']}
        >
            <UploadPics/>
        </ReactDependentScript>
    </LoginNeeded>
)
  
export default UploadPicsPage;