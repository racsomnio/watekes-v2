import Link from 'next/link';
// import Login from '../components/Login';

const Account = props => (
    <>
        <h1>Account</h1>
        <ul>
            <li>
                <Link prefetch href="/edit-account">
                    <a>Edit Account</a>
                </Link>
            </li>
            <li>Massage Matched: See who wants to start a massage session after you both think you guys are Rubbable</li>
            <li>Rubs #: You can offer only one Rub per day. If you don't use it you lose it.</li>
            <li>Set the distance: I want to look within a distance of..</li>
            <li>Change location</li>
            <li>Invite Friends: For every friend who registers you will get 5 free "Rubs"</li>
            
            {/* Fancy */}
            <li>Am I rubbable?: See who is open for a session with me.</li>
            <li>Let me see everybody's photos: Please upgrade to Fancy or Picky plan. Otherwise you can only see 1 pic from every user.</li>
            <li>Vacation Mode: if true, user will be able to see that you are a visitor, show airplane icon</li>
            <li>Age Range: Show me only people between this age.</li>
            <li>Ads Free</li>

            {/*  Picky */}
            <li>Young Forever: Hide my age</li>
            <li>I want to be found only by [straights, gays, lesbians, trans]</li>
            <li>Put me into the closet: Make my pictures blurry, and show them only on request</li>
            <li>Rubbed: list of the ones you already massage .</li>
            <li>Not Rubbables: Wanna bring someone back from the beyond? show list</li>
            <li>Faves: list of your favorite ones</li>
            <li>Warnings: See list of people who has post a warning about me</li>
            <li>Rooms: List of rooms. Rooms for 2 hands massage, or 8 hands. The ones you created or have participated.</li>
            <li>Unblur Pics: Request to reveal the pictures </li>
            <li>Reveal pics: grant permission to see my pics [under request only, any one who request]</li>
        </ul>
    </>
  )
  
export default Account;