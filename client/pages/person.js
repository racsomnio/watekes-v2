import LoginNeeded from '../components/LoginNeeded';
import PersonFull from '../components/PersonFull';

const PersonPage = props => (
    <LoginNeeded>
        <PersonFull id={props.query.id}/>
    </LoginNeeded>
)

export default PersonPage;