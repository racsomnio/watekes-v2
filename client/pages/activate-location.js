import LoginNeeded from '../components/LoginNeeded';
import AskLocation from '../components/AskLocation';

const ActivateLocation = () => (
    <LoginNeeded>
      <AskLocation/>
      <p>Problems on Safari?</p>
      <small>If you can't see a pop up asking for allowing your position, you can try the following:</small>
      <p><small>
        <ol>
          <li>Go to your mobile <strong>Settings</strong></li>
          <li>Look for <strong>Privacy</strong></li>
          <li>Click on <strong>Location Services</strong></li>
          <li>Scroll down and click on <strong>Safari</strong></li>
          <li>Select <strong>Ask</strong> or <strong>While using the App</strong></li>
        </ol>
      </small></p>
    </LoginNeeded>
  )
  
export default ActivateLocation