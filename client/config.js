// This is client side config only - don't put anything in here that shouldn't be public!
export const endpoint = `http://localhost:4444`;
export const prodEndpoint = `https://api.watekes.com`;
export const devWs = `ws://localhost:4444/`;
export const prodWs = `wss://api.watekes.com`;

export const perPage = 4;
